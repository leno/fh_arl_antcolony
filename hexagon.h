#pragma once
#include <SFML\Graphics.hpp>
#include <vector>

struct Position
{
	int posX;
	int posY;
};

class Hexagon
{
public:
	Hexagon(){
		isFood = false;
		isStart = false;
		isBarrier = false;
		pheromonPower = 0.0;
		oldPheromonPower = 0.0;
	}
	
	sf::CircleShape* hex;
	float pheromonPower;
	float oldPheromonPower;
	std::vector<Hexagon>* getNeigbours();
	bool isFood;
	bool isStart;
	bool isBarrier;
	Position position;

private:

};