#include "ant.h"
#include <iostream>
Ant::Ant(Hexagon** hexField, int fieldWithHeight, Position startPos, float optimalDistance, int* finishedAnts)
{

	this->hexfield = hexField;
	this->fieldWidthHight = fieldWithHeight;
	this->startPos = startPos;
	this->optimalDistance = optimalDistance;
	this->finishedAnts = finishedAnts;

	setStartPos();
	//push first ant pos
	visited.push_back(antpos);

	if (antpos.posY % 2 == 0){
		offset = 0;
	}
	else{
		offset = 1;
	}
	currNeigbours.reserve(6);
	srand(1523);
}

Ant::~Ant()
{
}

void Ant::moveRandom(){
	// check available options
	checkCurrNeighbours();
	
	// Choose one option
	if (currNeigbours.size() > 0){
		
		//Sum up pheromonstrength of all options
		float pheroSumNotVisited = 0.0;
		for (int i = 0; i < currNeigbours.size(); i++)
		{
			pheroSumNotVisited += currNeigbours.at(i)->pheromonPower;
		}

		// Sum up Distance of all Options
		float distSumNotVisited = currNeigbours.size() * constants::FIELD_DISTANCE;

		// Array to save direction probabilities
		float *directionsProbability = new float[currNeigbours.size()]();

		// Calculate probabilities
		for (int i = 1; i < currNeigbours.size(); ++i){
				float pheroStrenght = currNeigbours.at(i)->pheromonPower;

				double numerator = pow(pheroStrenght, constants::PHEROMON_AFFECT) * pow(constants::FIELD_DISTANCE, constants::DISTANCE_AFFECT);
				double denominator = pow(pheroSumNotVisited, constants::PHEROMON_AFFECT) * pow(distSumNotVisited, constants::DISTANCE_AFFECT);
				
				//probability p
				double p = numerator / denominator;

				//When denominator is 0 because there is no pheromon
				if (p == NAN){
					p = 0.0;
				}
				directionsProbability[i] = p;
		}

		// Add all prababilities (should equal 100)
		for (int i = 1; i < currNeigbours.size(); i++)
		{
			directionsProbability[i] += directionsProbability[i - 1];
		}

		// Choose a way
		int randomValue = rand() % 100;
		int directionIndex = rand() % currNeigbours.size(); // when all prababilities are 0 choose a random way

		for (int i = 0; i < currNeigbours.size(); i++)
		{
			if (directionsProbability[directionIndex] > randomValue)
				directionIndex = i;
		}
		antpos.posX = currNeigbours.at(directionIndex)->position.posX;
		antpos.posY = currNeigbours.at(directionIndex)->position.posY;
		visited.push_back(currNeigbours.at(directionIndex)->position);
	}else{
		currNeigbours.clear();
		visited.clear();
		setStartPos();
	}
}

void Ant::checkCurrNeighbours(){
	currNeigbours.clear();
	checkLeft();
	checkRight();
	checkDownL();
	checkDownR();
	checkUpL();
	checkUpR();

	// Check neighbours
	//int neighbourcount = currNeigbours.size();
	for (int i = 0; i < currNeigbours.size(); ++i){
		bool neighbourdeleted = false;
		if (currNeigbours.at(i)->isFood){
			visited.push_back(currNeigbours.at(i)->position);
			markVisited();
			visited.clear();
			setStartPos();
			//std::cout << "FOOD FOUND " << std::endl;
			return;
		}

		if (currNeigbours.at(i)->isStart){
			currNeigbours.erase(currNeigbours.begin() + i);
			i--;
			//std::cout << "is Start" << std::endl;
			neighbourdeleted = true;
		}

		if (!neighbourdeleted && currNeigbours.at(i)->isBarrier){
			currNeigbours.erase(currNeigbours.begin() + i);
			//std::cout << "is Barrier" << std::endl;
			neighbourdeleted = true;
			i--;
		}

		//Check if this field was already Visited
		if (!neighbourdeleted){
			for (int a = visited.size() - 1; a > 0; --a){
				if (!neighbourdeleted && visited.at(a).posX == currNeigbours.at(i)->position.posX && visited.at(a).posY == currNeigbours.at(i)->position.posY){
					currNeigbours.erase(currNeigbours.begin() + i);
					//std::cout << "is visited " << std::endl;
					neighbourdeleted = true;
					i--;
				}
			}
		}
	}
}

void Ant::setStartPos(){
	antpos.posX = startPos.posX;
	antpos.posY = startPos.posY;
	(*finishedAnts)++;
}

void Ant::markVisited(){
	//pheromonstrenght calculation (guessed optimale length / walked lenght)
	float pheromonstrenght = optimalDistance / visited.size();
	for (int i = 1; i < visited.size() - 1; ++i){
		float phreromonPower = hexfield[visited[i].posX][visited[i].posY].pheromonPower;
		hexfield[visited[i].posX][visited[i].posY].pheromonPower += pheromonstrenght/100.0;
		if (hexfield[visited[i].posX][visited[i].posY].pheromonPower > 1.0){
			hexfield[visited[i].posX][visited[i].posY].pheromonPower = 1.0;
		}
	}
}

Position Ant::getPosition(){
	return antpos;
}

std::vector<Position> Ant::getVisited(){
	return visited;
}

void Ant::checkLeft(){
	if (antpos.posX > 0){
		currNeigbours.push_back(&hexfield[antpos.posX - 1][antpos.posY]);
	}
}

void Ant::checkRight(){
	if (antpos.posX < fieldWidthHight - 1){
		currNeigbours.push_back(&hexfield[antpos.posX + 1][antpos.posY]);
	}
}

void Ant::checkUpL(){
	if (antpos.posY % 2 == 0){
		if (antpos.posY > 0 && antpos.posX > 0){
			currNeigbours.push_back(&hexfield[antpos.posX - 1][antpos.posY - 1]);
		}
	}
	else{
		if (antpos.posY > 0){
			currNeigbours.push_back(&hexfield[antpos.posX][antpos.posY - 1]);
		}
	}

}

void Ant::checkUpR(){
	if (antpos.posY % 2 == 0){
		if (antpos.posY > 0){
			currNeigbours.push_back(&hexfield[antpos.posX][antpos.posY - 1]);
		}
	}
	else{
		if (antpos.posX < fieldWidthHight - 1 && antpos.posY > 0){
			currNeigbours.push_back(&hexfield[antpos.posX + 1][antpos.posY - 1]);
		}
	}
}

void Ant::checkDownL(){
	if (antpos.posY % 2 == 0){
		if (antpos.posX > 0 && antpos.posY < fieldWidthHight - 1){
			currNeigbours.push_back(&hexfield[antpos.posX - 1][antpos.posY + 1]);
		}
	}
	else{
		if (antpos.posY < fieldWidthHight - 1){
			currNeigbours.push_back(&hexfield[antpos.posX][antpos.posY + 1]);
		}
	}

}

void Ant::checkDownR(){
	if (antpos.posY % 2 == 0){
		if (antpos.posY < fieldWidthHight - 1){
			currNeigbours.push_back(&hexfield[antpos.posX][antpos.posY + 1]);
		}
	}
	else{
		if (antpos.posX < fieldWidthHight - 1 && antpos.posY < fieldWidthHight - 1){
			currNeigbours.push_back(&hexfield[antpos.posX + 1][antpos.posY + 1]);
		}
	}

}