
#include <SFML\Graphics.hpp>
#include "hexagon.h"
#include "ant.h"
#include <Windows.h>
#include <iostream>




float calcGuessOptimalDistance(Position start, Position end){
	int xDist = start.posX - end.posX;
	int yDist = start.posY - end.posY;
	return std::sqrtf(std::pow(xDist, 2) + std::pow(yDist, 2));
}

int main(){
	sf::RenderWindow window(sf::VideoMode(800, 600, 32), "Ant Colony");
	window.setFramerateLimit(60); // Set a framrate

	int hexradius = 5;
	int hexDurchmesser = 2 * hexradius;
	int fieldWidthHeight = (hexradius *14);
	int offset = 0;
	float evaporationTime = 1.0;
	int* finishedAnts = new int();
	
	Hexagon** hexfield = new Hexagon*[fieldWidthHeight];
	for (int i = 0; i < fieldWidthHeight; ++i){
		hexfield[i] = new Hexagon[fieldWidthHeight];
		for (int j = 0; j < fieldWidthHeight; ++j){
			if (j % 2 == 0){
				hexfield[i][j].hex = new sf::CircleShape();
				hexfield[i][j].hex->setRadius(hexradius);
				hexfield[i][j].hex->setPointCount(6);
				hexfield[i][j].hex->setFillColor(sf::Color::White);
				hexfield[i][j].hex->setPosition(i * hexDurchmesser, (j * hexDurchmesser) + offset);
				hexfield[i][j].position.posX = i;
				hexfield[i][j].position.posY = j;
				offset -= 2;
			}
			else{
				hexfield[i][j].hex = new sf::CircleShape();
				hexfield[i][j].hex->setRadius(hexradius);
				hexfield[i][j].hex->setPointCount(6);
				hexfield[i][j].hex->setFillColor(sf::Color::White);
				hexfield[i][j].hex->setPosition((i * hexDurchmesser) + hexradius, (j * hexDurchmesser) + offset);
				hexfield[i][j].position.posX = i;
				hexfield[i][j].position.posY = j;
				offset -= 2;				
			}
			
		}
		offset = 0;
	}

	Hexagon* startField = &hexfield[40][40];
	startField->hex->setFillColor(sf::Color::Blue);
	startField->isStart = true;

	Hexagon* foodField = &hexfield[69][0];
	foodField->hex->setFillColor(sf::Color::Red);
	foodField->isFood = true;

	float optimalDist = calcGuessOptimalDistance(startField->position, foodField->position);

	std::vector<Ant> ants(constants::ANT_COUNT, Ant(hexfield, fieldWidthHeight, startField->position, optimalDist, finishedAnts));

	int antRunCount = 0;
	bool applyEvaporation = false;

	while (window.isOpen())
	{
		//Input Handling
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::MouseButtonReleased:
				if (event.mouseButton.button == sf::Mouse::Left){
					printf("%d , %d \n", sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
					int mouseX = sf::Mouse::getPosition(window).x /10;
					int mouseY = sf::Mouse::getPosition(window).y/ 8;

					if (mouseX < 0 || mouseX >69)
						break;

					if (mouseY < 0 || mouseY > 69)
						break;
					foodField->isFood = false;
					foodField = &hexfield[mouseX][mouseY];
					foodField->isFood = true;
				}
				else if (event.mouseButton.button == sf::Mouse::Right){
					int mouseX = sf::Mouse::getPosition(window).x / 10;
					int mouseY = sf::Mouse::getPosition(window).y / 8;

					if (mouseX < 0 || mouseX >69)
						break;

					if (mouseY < 0 || mouseY > 69)
						break;
					if (hexfield[mouseX][mouseY].isBarrier){
						hexfield[mouseX][mouseY].isBarrier = false;
					}
					else{
						hexfield[mouseX][mouseY].isBarrier = true;
					}
				}
				break;
			default:
				break;
			}

		}
		
		// multiple ants run at the same time
		for (int i = 0; i < ants.size(); ++i){
			ants.at(i).moveRandom();
		}

		if (*finishedAnts >= constants::ANT_COUNT){
			*finishedAnts = 0;
			applyEvaporation = true;
		}
		
		window.clear(sf::Color(100, 100, 200));
		for (int i = 0; i < (fieldWidthHeight); ++i){
			for (int j = 0; j < (fieldWidthHeight); ++j){
				float pheromonPower = hexfield[i][j].pheromonPower;
				//after each antcount ants apply evaporation
				if (applyEvaporation){
					if (pheromonPower > 0.0){
						float pheromonPowerOld = hexfield[i][j].oldPheromonPower;
						float pheromonpowerDelta = hexfield[i][j].pheromonPower - pheromonPowerOld;

						float pheromonpowerNew = (1 - constants::REMEMBER_VALUE) * pheromonPowerOld + pheromonpowerDelta;
						hexfield[i][j].pheromonPower = pheromonpowerNew;
						hexfield[i][j].oldPheromonPower = pheromonpowerNew;
					}
					
				}
				pheromonPower = hexfield[i][j].pheromonPower;
				if (pheromonPower > 1.0){
					printf("%d", pheromonPower);
				}

				hexfield[i][j].hex->setFillColor(sf::Color::Color(255, 255, 255, 255 - (int)(255 * pheromonPower)));
				if (hexfield[i][j].isBarrier){
					hexfield[i][j].hex->setFillColor(sf::Color::Green);
				}
				startField->hex->setFillColor(sf::Color::Blue);
				foodField->hex->setFillColor(sf::Color::Red);

				window.draw(*hexfield[i][j].hex);
			}
		}
		applyEvaporation = false;

		window.display();
		//Sleep(3000);
		
	}
	return 0;

}