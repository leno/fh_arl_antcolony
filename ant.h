#include<vector>
#include "hexagon.h"

namespace constants{
	const int ANT_COUNT = 10000;
	const float REMEMBER_VALUE = 0.5;
	const float DISTANCE_AFFECT = 0.1;
	const float PHEROMON_AFFECT = 0.9;
	const float FIELD_DISTANCE = 1.0;
}

class Ant
{
public:
	Ant(Hexagon**,int, Position, float, int*);
	~Ant();

	void moveRandom();

	Position getPosition();
	std::vector<Position> getVisited();
	float optimalDistance;;

private:
	Position startPos;
	Position endPos;
	int offset;
	bool foodFound;
	Position antpos;
	std::vector<Position> visited;
	std::vector<Hexagon*> currNeigbours;
	Hexagon** hexfield;
	int fieldWidthHight;

	int* finishedAnts;
	void checkCurrNeighbours();
	void checkLeft();
	void checkRight();
	void checkUpL();
	void checkUpR();
	void checkDownL();
	void checkDownR();

	void markVisited();
	void setStartPos();

};

